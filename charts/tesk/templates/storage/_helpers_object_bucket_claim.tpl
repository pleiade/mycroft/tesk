{{/*
Retrieve bucket endpoint from ObjectBucketClaim config map
*/}}
{{- define "tesk.obc.config" -}}
{{- with (lookup "v1" "ConfigMap" .Release.Namespace .Values.bucket.obc_name).data -}}
[default]
endpoint_url=http://{{ .BUCKET_HOST }}:{{ .BUCKET_PORT }}
{{- end }}
{{- end }}

{{/*
Retrieve bucket endpoint from ObjectBucketClaim secret
*/}}
{{- define "tesk.obc.credentials" -}}
{{ with (lookup "v1" "Secret" .Release.Namespace .Values.bucket.obc_name).data -}}
[default]
aws_access_key_id={{ .AWS_ACCESS_KEY_ID | b64dec }}
aws_secret_access_key={{ .AWS_SECRET_ACCESS_KEY | b64dec }}
{{- end }}
{{- end }}
