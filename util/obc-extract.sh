#!/bin/sh

# Run this in charts/tesk/s3-config to extract s3 config and credentials
# from a provisioned ObjectBucketClaim.

OBC=${1:-tesk-shared}

kubectl get configmap/${OBC} \
	-o go-template='[default]{{"\n"}}endpoint_url=http://{{.data.BUCKET_HOST}}:{{.data.BUCKET_PORT}}{{"\n"}}' \
	> config

kubectl get secret/${OBC} \
	-o go-template='[default]{{"\n"}}aws_access_key_id={{.data.AWS_ACCESS_KEY_ID|base64decode}}{{"\n"}}aws_secret_access_key={{.data.AWS_SECRET_ACCESS_KEY|base64decode}}{{"\n"}}' \
	> credentials
